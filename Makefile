# This Makefile was heavily inspired by the Makefile of the package git2r
# (Version: 0.18.0.9000)

# Determine package name and version from DESCRIPTION file
PKG_VERSION=$(shell grep -i ^version DESCRIPTION | cut -d : -d \  -f 2)
PKG_NAME=$(shell grep -i ^package DESCRIPTION | cut -d : -d \  -f 2)

# Name of built package
PKG_TAR=$(PKG_NAME)_$(PKG_VERSION).tar.gz

#data-raw
DR=./data-raw

# Install package
install: data
	cd .. && R CMD INSTALL $(PKG_NAME)

# Download data
data: data-raw/*
	wget 'https://zenodo.org/record/1039980/files/panormos_survey-data_0-1-0.zip' -O $(DR)/survey-data.zip
	unzip -o $(DR)/survey-data.zip -d $(DR)/survey-data
	rm $(DR)/survey-data.zip
	wget 'https://zenodo.org/record/1185044/files/panormos_gis-copernicus_0-1-0.zip' -O $(DR)/gis-copernicus.zip
	unzip -o $(DR)/gis-copernicus.zip -d $(DR)/gis-copernicus
	rm $(DR)/gis-copernicus.zip 
	Rscript --vanilla  $(DR)/data-make.R

# Build documentation with roxygen
# 1) Remove old doc
# 2) Generate documentation
roxygen:
	rm -f man/*.Rd
	cd .. && Rscript -e "library(roxygen2); roxygenize('$(PKG_NAME)')"

# Generate PDF output from the Rd sources
# 1) Rebuild documentation with roxygen
# 2) Generate pdf, overwrites output file if it exists
pdf: roxygen
	cd .. && R CMD Rd2pdf --force $(PKG_NAME)

# Build and check package
check:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && _R_CHECK_CRAN_INCOMING_=FALSE NOT_CRAN=true \
        R CMD check $(PKG_TAR)

check-no-vignette:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --no-vignettes --no-build-vignettes $(PKG_TAR)

check-as-cran:
	cd .. && R CMD build $(PKG_NAME)
	cd .. && R CMD check --as-cran $(PKG_TAR)
