# The rkeos.data.ppas package

Status: [![pipeline status](https://gitlab.com/rkeos/rkeos.data.ppas/badges/master/pipeline.svg)](https://gitlab.com/rkeos/rkeos.data.ppas/commits/master)

This package contains some of datasets of the Panormos Project Archaeological Survey.

> See online: http://www.panormos.de

or

> Strupler, N. & Wilkinson, T. C. Reproducibility in the Field:
> Transparency, Version Control and Collaboration on the Project
> Panormos Survey Open Archaeology, 2017, 3, 279-304
> https://doi.org/10.1515/opar-2017-0019
>
> The Project Panormos Survey grew out of an earlier three-year rescue
> excavation at a site near the presumed ancient Ionian harbour town
> of Panormos (modern Mavişehir, Aydın, Turkey), the port-of-entry for
> the oracle sanctuary at Didyma (modern Didim), which had revealed a
> densely-used necropolis dating to the 7th and 6th centuries BC. The
> project, a collaboration between the local archaeological museum at
> Milet (Balat) and the German Archaeological Institute (or DAI,
> Istanbul), led in the field by Dr. Anja Slawisch (DAI), set out from
> the beginning to maintain a wide research agenda. To address the
> horizontal extent of the necropolis, to determine its relationship
> to any potential contemporary settlement in the area, and to place
> the Panormos necropolis, in 2015 Project Panormos expanded its brief
> to an intensive survey.  Methodologically, the aim was to adopt some
> of the established intensive field-walking techniques which have
> long been applied in other parts of the Aegean and in the wider
> Mediterranean (Alcock & Cherry 2004), but still have been used by a
> very small proportion of survey projects across Turkey and ancient
> Asia Minor (with some exceptions: Ersoy & Koparal 2008, Ersoy, Tuna,
> & Koparal 2010, Matthews & Glatz 2009, Düring & Glatz 2016). For the
> first season (2015), the experiences from which this article is
> based, it was decided to focus on the region immediately around the
> archaic necropolis in order to identify the extent of the site and
> to identify any nearby settlements and landscape usage. Given the
> relative proximity of a major Bronze Age site of Tavşan Adası, the
> survey also hoped to identify diachronic landscape use over the long
> term. A considerable amount of ground was covered in 2015 using a
> GPS-based grid that allowed relatively regular sampling shapes and
> hence fast tract recording (Figure 1).


The source of the package (https://gitlab.com/rkeos/rkeos.data.ppas)
contains a folder `data-raw` and a script `data-make.R` explaining
which data-sets have been used and how it was packaged.

